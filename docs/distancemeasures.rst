
Distance Measures - Converting Absolute Coordinates to Pairwise Similarities
-----------
The distance measures are used when converting absolute datasets (containing absolute
coordinates) to relative datasets (pairwise similarities). The distance measures define
how to assess the similarity between a pair of objects given their absolute coordinates.

ClustEval ships with a standard set of distance measures. For a list of available clustering quality measures see `here <https://gitlab.compbio.sdu.dk/clusteval/components/distance-measures>`_

Check :ref:`clusteval_extend_distance_measures` for more information on how to extend the framework by new distance
measures.
