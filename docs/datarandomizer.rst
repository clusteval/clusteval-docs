Data Randomizer - Distorting Existing Data Sets
===============

ClustEval provides means to distort data sets. For a list of all available data randomizers head over `here <https://gitlab.compbio.sdu.dk/clusteval/components/data-randomizer>`_.

Check :ref:`clusteval_extend_data_randomizers` for more information on how to extend ClustEval by new data randomizer.