
Clustering Quality Measures - Is It a Good Clustering?
-------------
Clustering quality measures assess the quality of calculated clusterings. 

ClustEval ships with a standard set of clustering quality measures. For a list of available clustering quality measures see `here <https://gitlab.compbio.sdu.dk/clusteval/components/clustering-quality-measures>`_

Check :ref:`clusteval_extend_quality_measures` for more information on how to extend ClustEval by new clustering quality measures.
