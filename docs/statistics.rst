Statistics - Analyzing Data & Clusterings
==========

Analysis runs are analyzing certain aspects, called statistics, of the data set or clustering of interest.

Depending on what is being analyzed ClustEval distinguishes between different sub types of analysis runs and statistics:

* **Data analysis run / Data statistics** are being used to analyze a data set
* **Run analysis run / Run statistics** are being used to analyze results of a run, for example clustering performances of methods.
* **Run-data analysis run / Run-data statistics** are being used to analyze results of a data analysis run together with results of a run analysis run. An example is, to train a  linear regression models on data statistics and clustering method performances of a data set.