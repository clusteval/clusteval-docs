Data Set Types - Classifying Data
==============

Each data set is required to have a type. This type is used by ClustEval to group data sets of similar types when representing results.

For a list of all data set types head over `here <https://gitlab.compbio.sdu.dk/clusteval/components/data-dataset-type>`_.

Check :ref:`clusteval_extend_dataset_types` for more information on how to extend ClustEval by types.