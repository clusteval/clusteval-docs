.. _extendingclusteval:

Extending ClustEval
========
ClustEval can be extended in different ways. The following sections will show you, which functionality you can add to the framework and how.

.. _clusteval_extend_programs:

Clustering Methods
--------
As explained in :ref:`clusteringmethods` clusteval supports two different kinds of clustering methods: Stan-
dalone programs and R programs. Standalone programs are those, for which you have
to provide an executable file which then will be executed by the framework. R programs
are methods implemented in R, which will be invoked by clusteval by using the Rserve
interface.

Adding Standalone Programs
^^^^^^^^^
See the API documentation of :java:ref:`de.clusteval.program.StandaloneProgram` on information about how to extend ClustEval.

Writing R Programs
^^^^^^^^^
See the API documentation of :java:ref:`de.clusteval.program.r.RProgram` on information about how to extend ClustEval.

.. _clusteval_extend_dataset_types:

Writing Data Set Types
--------
See the API documentation of :java:ref:`de.clusteval.data.dataset.type.DataSetType` on information about how to extend ClustEval.

.. _clusteval_extend_formats:

Formats
--------

Writing Data Set Formats
^^^^^^^^^
See the API documentation of :java:ref:`de.clusteval.data.dataset.format.DataSetFormat` on information about how to extend ClustEval.

Writing Run Result Formats
^^^^^^^^^
See the API documentation of :java:ref:`de.clusteval.run.runresult.format.RunResultFormat` on information about how to extend ClustEval.


.. _clusteval_extend_parameter_optimization_method:

Writing Parameter Optimization Methods
--------
See the API documentation of :java:ref:`de.clusteval.cluster.paramOptimization.ParameterOptimizationMethod` on information about how to extend ClustEval.


.. _clusteval_extend_distance_measures:

Writing Distance Measures
--------
See the API documentation of :java:ref:`de.clusteval.data.distance.DistanceMeasure` on information about how to extend ClustEval.

.. _clusteval_extend_quality_measures:

Writing Clustering Quality Measures
--------
See the API documentation of :java:ref:`de.clusteval.cluster.quality.ClusteringQualityMeasure` on information about how to extend ClustEval.

Writing Statistics
--------
ClustEval can analyze properties of clusterings, data sets and relationship between the two. We call such properties run, data and run-data statistics respectively. 

For more information about how to extend ClustEval with your own statistics have a look at

* the API documentation of :java:ref:`de.clusteval.run.statistics.RunStatistic`,
* the API documentation of :java:ref:`de.clusteval.data.statistics.DataStatistic`,
* the API documentation of :java:ref:`de.clusteval.run.statistics.RunDataStatistic`.

.. _clusteval_extend_data_preprocessors:

Writing Data Preprocessors
--------
See the API documentation of :java:ref:`de.clusteval.data.preprocessing.DataPreprocessor` on information about how to extend ClustEval.


.. _clusteval_extend_data_randomizers:

Writing Data Randomizers
--------
See the API documentation of :java:ref:`de.clusteval.data.randomizer.DataRandomizer` on information about how to extend ClustEval.


.. _clusteval_extend_dataset_generators:

Writing Data Set Generators
--------
See the API documentation of :java:ref:`de.clusteval.data.dataset.generator.DataSetGenerator` on information about how to extend ClustEval.