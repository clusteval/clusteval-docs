import sys, os

import sys
import os

# on_rtd is whether we are on readthedocs.org, this line of code grabbed from docs.readthedocs.org
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'

sys.path.append(os.path.abspath('sphinxext'))

extensions = [
  'sphinx.ext.autodoc',
  'javasphinx',
  'sphinx.ext.imgmath'
]

# The suffix of source filenames.
source_suffix = '.rst'

master_doc = 'index'


# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

project = u'ClustEval'
author = u'Christian Wiwie'

# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
if not on_rtd:  # only import and set the theme if we're building docs locally
    import sphinx_rtd_theme
    html_theme = 'sphinx_rtd_theme'
    html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
else:
    html_theme = 'default'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

javadoc_url_map = {
    'de.clusteval' : ('http://clusteval.pages.compbio.sdu.dk/clusteval-lib/apidocs', 'javadoc8'),
}
